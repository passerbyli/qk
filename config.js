//作者: Sail QQ:151263555
exports.debug = false

//端口
exports.port = 8380

//session失效时间
exports.sessionTimeout = 1800

exports.mapping = function (ph) {
	if(ph === "" || ph === "/") return "/web/sys/mainFrame.html";
	if(ph === "/favicon.ico") return "/web/img/favicon.ico";
	return ph;
}

//缓存时间,默认10年
exports.cacheTimeUrl = function (ph) { return 315360000000; }

//是否禁用WebSocket
exports.dsblWk = true

//sessionId存储的方式,js,header,cookie
exports.sessionId = "header"

//日志配置 lever:日志等级info log error,path日志保存的路径
//exports.log = {lever: "error",path: "c:/"}

//工程所在路径
exports.projectPath = __dirname

if (process.argv[2]) {
	var argv = eval('('+process.argv[2]+')');
	for(var key in argv) exports[key] = argv[key];
}
process.title = "qk";
