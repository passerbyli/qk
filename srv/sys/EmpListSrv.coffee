{SysSrv} = require "./SysSrv"
exports.EmpListSrv = new Class
  Extends: SysSrv
  options:
    tab: "emp"
  dataGrid: (reqOpt,pgOffset,pgNum,tab)->
    t = this
    o = t.options
    pgOffset = Number pgOffset
    pgOffset = 0 if isNaN pgOffset
    pgNum = Number pgNum
    pgNum = 0 if isNaN pgNum
    return [] if pgNum is 0
    tab = tab or o.tab
    tabEd = t.escapeId tab
    argArr = [pgNum,pgOffset]
    sql = """
    select t.*
      ,d.lbl as "d.lbl"
      ,r.lbl as "r.lbl"
    from #{tabEd} t
    left join dept d
      on d.id=t.dept_id
    left join role r
      on r.id=t.role_id
    """
    sql += t.srch2Whr reqOpt,argArr,o.seaArr,true
    sql += " order by "
    sql += t.sort2Ody reqOpt,o.sortArr
    sql += "\"t\".\"id\" asc"
    sql += " limit $1 offset $2"
    rltSet = yield t.callSql reqOpt,sql,argArr
    rltSet = rltSet.rows
    rltSet
  dataCount: (reqOpt,tab)->
    t = this
    o = t.options
    tab = tab or o.tab
    tabEd = t.escapeId tab
    argArr = []
    sql = """
    select count(t.id) as count
    from #{tabEd} t
    left join dept d
      on d.id=t.dept_id
    left join role r
      on r.id=t.role_id
    """
    sql += t.srch2Whr reqOpt,argArr,o.seaArr,true
    rltSet = yield t.callSql reqOpt,sql,argArr
    rltSet = rltSet.rows
    rltSet[0].count
