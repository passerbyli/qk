{SysSrv} = require "./SysSrv"
{MainFrameSrv} = require "./MainFrameSrv"
exports.MenuAddSrv = new Class
  Extends: SysSrv
  options:
    tab: "menu"
  "@confirmButClk":{isTrann:true,comment:"点击确定按钮"}
  confirmButClk: (reqOpt,entry,keyArr,returning,tab)->
    t = this
    rltSet = yield t.treInsert reqOpt,entry,keyArr,returning,tab
    rltSet
  #获得菜单对应的孩子
  menuCld: (reqOpt,id)->
    t = this
    sql = """
    select m.*
    from menu m 
    join menu_tree_path mp 
      on m.id=mp.descendant
    where 
      mp.ancestor=$1
      and mp.path_length=1
      and m.enable=true
      and m.is_leaf=false
    order by m.sort_num desc
    """
    rltSet = yield t.callSql reqOpt,sql,[id]
    rltSet = rltSet.rows
    rltSet
  #获得菜单的根节点,可能有很多个
  menuRoot: (reqOpt)->
    t = this
    sql = """
    select m.*
    from menu m 
    where 
      m.is_root=true
      and m.enable=true
      and m.is_leaf=false
    order by m.sort_num asc
    """
    rltSet = yield t.callSql reqOpt,sql
    rltSet = rltSet.rows
    rltSet
  #获得菜单对应的孩子
  pageCld: (reqOpt,id)->
    t = this
    sql = """
    select m.*
    from menu m 
    join menu_tree_path mp 
      on m.id=mp.descendant
    where 
      mp.ancestor=$1
      and mp.path_length=1
      and m.enable=true
    order by m.sort_num asc
    """
    rltSet = yield t.callSql reqOpt,sql,[id]
    rltSet = rltSet.rows
    rltSet
  #获得菜单的根节点,可能有很多个
  pageRoot: (reqOpt)->
    t = this
    sql = """
    select m.*
    from menu m 
    where 
      m.is_root=true
      and m.enable=true
    """
    rltSet = yield t.callSql reqOpt,sql
    rltSet = rltSet.rows
    rltSet
  findMenuByPageId: (reqOpt,page_id)->
    t = this
    sql = "select * from menu where page_id=$1 limit 1 offset 0"
    rltSet = yield t.callSql reqOpt,sql,[page_id]
    rltSet = rltSet.rows
    rltSet[0]
