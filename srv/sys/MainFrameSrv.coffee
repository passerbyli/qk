{SysSrv} = require "./SysSrv"

exports.MainFrameSrv = new Class(
  Extends: SysSrv
  #获得菜单对应的孩子
  menuCld: (reqOpt,menuId)->
    t = this
    sql = """
    select m.*
    from menu m 
    join menu_tree_path mp 
      on m.id=mp.descendant
    where 
      mp.ancestor=$1
      and mp.path_length=1
      and m.enable=true
    order by m.sort_num asc
    """
    rltSet = yield t.callSql reqOpt,sql,[menuId]
    rltSet = rltSet.rows
    rltSet
  #获得菜单的根节点,可能有很多个
  menuRoot: (reqOpt)->
    t = this
    sql = """
    select m.*
    from menu m 
    where 
      m.is_root=true
      and m.enable=true
    order by m.sort_num asc
    """
    rltSet = yield t.callSql reqOpt,sql
    rltSet = rltSet.rows
    rltSet
)