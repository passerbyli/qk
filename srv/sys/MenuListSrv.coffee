{SysSrv} = require "./SysSrv"
exports.MenuListSrv = new Class
  Extends: SysSrv
  options:
    tab: "menu"
  dataGrid: (reqOpt,pgOffset,pgNum,tab)->
    t = this
    o = t.options
    pgOffset = Number pgOffset
    pgOffset = 0 if isNaN pgOffset
    pgNum = Number pgNum
    pgNum = 0 if isNaN pgNum
    return [] if pgNum is 0
    tab = tab or o.tab
    tabEd = t.escapeId tab
    argArr = [pgNum,pgOffset]
    sql = """
    select t.*
      ,(select mp.ancestor from menu_tree_path mp where t.id=mp.descendant and mp.path_length=1) as prn_id
      ,p.lbl as "p.lbl"
      ,(case when t.open_op='tab' then '选项卡' when t.open_op='modal' then '模态窗口' else t.open_op end) as _open_op
    from menu t
    left join page p
      on p.id=t.page_id
    """
    sql += t.srch2Whr reqOpt,argArr,o.seaArr,true
    sql += " order by "
    sql += t.sort2Ody reqOpt,o.sortArr
    sql += "\"t\".\"sort_num\" asc"
    sql += " limit $1 offset $2"
    rltSet = yield t.callSql reqOpt,sql,argArr
    rltSet = rltSet.rows
    rltSet
  dataCount: (reqOpt,tab)->
    t = this
    o = t.options
    tab = tab or o.tab
    tabEd = t.escapeId tab
    argArr = []
    sql = """
    select count(t.id) as count
    from #{tabEd} t
    left join page p
      on p.id=t.page_id
    """
    sql += t.srch2Whr reqOpt,argArr,o.seaArr,true
    rltSet = yield t.callSql reqOpt,sql,argArr
    rltSet = rltSet.rows
    rltSet[0].count
