{SysSrv} = require "./SysSrv"

exports.LoginSrv = new Class(
  Extends: SysSrv
  onDraw: ->
    t = this
    yield SysSrv.prototype.onDraw.apply t,arguments
    return
  "@login":{isTran:true}
  login: (reqOpt,code,password)->
    t = this
    sql = "select * from emp where code=$1 and password=$2 and enable=true"
    rltSet = yield t.callSql reqOpt,sql,[code,password]
    rltSet = rltSet.rows
    suc = rltSet.length isnt 0
    errMsg = null
    if suc is false
      errMsg = "用户名或密码不正确!"
      t.session.emp = undefined
    else
      t.session.emp = rltSet[0]
    {suc:suc,errMsg:errMsg}
)