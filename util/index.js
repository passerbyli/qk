var path = require("path");
global._PROJECT_PATH = path.dirname(__dirname)+"/";
require("m_");
require("date");
require("ExpandUtil");
require("coffee-script").register();
var config = require("../config");

require.extensions['.node_'+process.platform+"_"+process.arch] = function(module, filename) {
	return require.extensions['.node'](module, filename);
};

require("logjs").init(config.log);

require("./Main");