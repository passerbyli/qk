config = require "../config"
url = require "url"
fsAsync = require "fsAsync"
path = require "path"
zlibAsync = require "zlibAsync"
crypto = require "crypto"

_PROJECT_PATH = global._PROJECT_PATH

mime = {
  "css": "text/css; charset=UTF-8"
  "gif": "image/gif"
  "html": "text/html; charset=UTF-8"
  "ico": "image/x-icon"
  "jpeg": "image/jpeg"
  "jpg": "image/jpeg"
  "js": "text/javascript; charset=UTF-8"
  "json": "application/json; charset=UTF-8"
  "pdf": "application/pdf"
  "xlsx": "application/vnd.ms-excel"
  "png": "image/png"
  "svg": "image/svg+xml; charset=UTF-8"
  "swf": "application/x-shockwave-flash"
  "tiff": "image/tiff"
  "txt": "text/plain; charset=UTF-8"
  "wav": "audio/x-wav"
  "wma": "audio/x-ms-wma"
  "wmv": "video/x-ms-wmv"
  "xml": "text/xml; charset=UTF-8"
}
exports.mime = mime
gzip = {
  css: true
  txt: true
  json: true
  xml: true
  js: true
  html: true
  csv: true
  svg: true
}
exports.gzip = gzip

md5Etag = (buffer,ctEn,req,res)->
  res.setHeader "Content-Encoding",ctEn if ctEn isnt undefined and ctEn isnt null
  res.setHeader "Content-Length", buffer.length
  if buffer.length > 400
    inm = req.headers["if-none-match"]
    hashMd5 = crypto.createHash "md5"
    md5Str = hashMd5.update(buffer).digest "base64"
    md5Str = md5Str.substring 0,md5Str.length-2
    if md5Str is inm
      res.writeHead 304
    else
      res.setHeader "Etag",md5Str
      res.writeHead 200
      res.write buffer
  else
    res.writeHead 200
    res.write buffer
  res.end()
  return
exports.md5Etag = md5Etag

gzipFn = (aed,extname,buffer)->
  zipType = null
  zipBuf = null
  return {zipType:null,zipBuf:buffer} if buffer.length < 2048 or aed is undefined or aed is null
  if aed.match(/\bgzip\b/) and extname isnt null and extname.trim() isnt "" and gzip[extname] isnt undefined
    zipType = "gzip"
    zipBuf = yield zlibAsync.gzipAsync buffer
  else if aed.match(/\bdeflate\b/) and extname isnt null and extname.trim() isnt "" and gzip[extname] isnt undefined
    zipType = "deflate"
    zipBuf = yield zlibAsync.deflateAsync buffer
  else
    zipBuf = buffer
  {zipType:zipType,zipBuf:zipBuf}
exports.gzipFn = gzipFn

exports.resFile = (ph,req,res)->
  extname = path.extname ph
  extname = extname.substring 1,extname.length if extname
  res.setHeader "Content-Type",mime[extname] if extname and mime[extname]
  filename = path.basename ph
  disFilename = filename
  buffer = null
  src = null
  if config.debug
    if extname is "js"
      if ph is "/web/js/res/_resAll.js"
        Buffers = require "buffers"
        buffers = new Buffers()
        for item in _resAll
          try buffer = yield fsAsync.readFileAsync _PROJECT_PATH+item catch err
          buffers.push buffer if buffer
        buffer = buffers.toBuffer()
      else
        src = undefined
        phCoffee = ph.substring(0,ph.lastIndexOf("."))+".coffee"
        try buffer = yield fsAsync.readFileAsync _PROJECT_PATH+phCoffee catch err
        if buffer
          src = buffer.toString()
          debug_open = "###debug"
          debug_close = "###"
          src = src.replace new RegExp(debug_open+"([\\s\\S]*?)"+debug_close,"g"),(str)-> str.substring(debug_open.length,str.length-debug_close.length).trim()
          coffeeScript = require "coffee-script"
          #ref = coffeeScript.compile src,{sourceMap: true,inline: true,sourceFiles: [phCoffee]}
          #src = ref.js+"\n//# sourceMappingURL=data:application/json;base64,"+(new Buffer(unescape(encodeURIComponent(ref.v3SourceMap)))).toString("base64")
          src = coffeeScript.compile src
        else
          try buffer = yield fsAsync.readFileAsync _PROJECT_PATH+ph catch err
          src = buffer.toString() if buffer
        if src
          defineFilter = ["\\/web\\/sys\\/.*","\\/web\\/js\\/comp\\/.*","\\/web\\/js\\/esl\\/seaEsl\\.js"]
          accept = false
          for filter in defineFilter
            if new RegExp(filter,"gm").test ph
              accept = true
              break
          src = "sea_define(function(require,exports,module){"+src+"});" if accept
          buffer = new Buffer src
    else if extname is "css"
      phLess = ph.substring(0,ph.lastIndexOf("."))+".less"
      try buffer = yield fsAsync.readFileAsync _PROJECT_PATH+phLess catch err
      if buffer
        src = buffer.toString()
        less = require "less"
        src = yield less.render src
        buffer = new Buffer src.css
    res.setHeader "Cache-Control","no-cache"
    res.setHeader "Pragma","no-cache"
  else
    ctu = 0
    if config.cacheTimeUrl
      ctu = config.cacheTimeUrl ph
    if ctu
      res.setHeader "Cache-Control", "max-age="+ctu
    else
      res.setHeader "Cache-Control","no-cache"
      res.setHeader "Pragma","no-cache"
  if buffer is null or buffer is undefined
    try buffer = yield fsAsync.readFileAsync _PROJECT_PATH+ph catch err
  res.setHeader "Content-Disposition","inline; filename="+disFilename
  if buffer is null or buffer is undefined
    res.writeHead 404
    res.end()
    return
  gf = yield gzipFn req.headers["accept-encoding"],extname,buffer
  md5Etag gf.zipBuf,gf.zipType,req,res
  return
if config.debug
  _resAll = [
    "/web/js/res/avalon.modern.js"
    "/web/js/res/co.js"
    "/web/js/res/mootools-core-1.5.0-full-nocompat.js"
    "/web/js/res/sea-debug.js"
    "/web/js/res/seajs-css-debug.js"
    "/web/js/res/mootools-more-1.5.0.js"
    "/web/js/res/config.js"
    "/web/js/res/DIY.js"
    "/web/js/res/setImmediate.js"
    "/web/js/res/ExpandUtil.js"
  ]