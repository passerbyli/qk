require "./MenuEdit.css"
{Srv} = require "Srv"
{MenuAdd} = require "./MenuAdd"

exports.MenuEdit = new Class
  Extends: MenuAdd
  onDraw: ->
    t = this
    o = t.options
    elt = o.ele
    yield MenuAdd.prototype.onDraw.apply t,arguments
    eny = elt.retrieve "eny"
    yield t.setPgVal eny
    t.setPgOldVal eny
    return
  initThisSrv: ->
    t = this
    o = t.options
    elt = o.ele
    o.thisSrv = new Srv clz:"sys."+o.enyStr+"EditSrv"
    o.thisSrv.options.ele = elt
    return
  confirmRst: (eny,headArr)->
    t = this
    o = t.options
    elt = o.ele
    eny0 = elt.retrieve "eny"
    eny.id = eny0.id
    rltSet = yield MenuAdd.prototype.confirmRst.apply t,[eny,headArr]
    rltSet
