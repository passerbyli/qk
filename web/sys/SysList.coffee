{Srv} = require "Srv"
{SysWin} = require "./SysWin"

exports["@SysList"] = {comment:"列表页面基类"}
exports.SysList = new Class
  Extends: SysWin
  options:
    enyStr: "Usr"
    headArr: []
    headObj:
      optLbl:"操作"
  onDraw: ->
    t = this
    yield SysWin.prototype.onDraw.apply t,arguments
  initThisSrv: ->
    t = this
    o = t.options
    elt = o.ele
    o.thisSrv = new Srv clz:"sys."+o.enyStr+"ListSrv"
    o.thisSrv.options.ele = elt
    return
  initPg: ->
    t = this
    o = t.options
    elt = o.ele
    yield t.initSearchList()
    yield t.initSort()
    yield t.initGrid()
    return
  "@initSort":{comment:"初始化排序事件"}
  initSort: ->
    t = this
    o = t.options
    elt = o.ele
    grid_tbl = elt.getE ".grid_tbl"
    thead = grid_tbl.getE "thead"
    thArr = thead.getEs "th"
    for i in [0...thArr.length]
      th = thArr[i]
      epx = undefined
      th.addEvent "mousedown",(e)->
        epx = e.page.x
        return
      th.addEvent "mouseup",(e)->
        if !epx or Math.abs(e.page.x-epx) < 5
          co t.thClk this,e
        return
      th.addEvent "dblclick",(e)-> e.stop()
    return
  "@syncSort":{comment:"扫描表头上的当前排序状态,同步后台上去,刷新表格,被thClk调用"}
  syncSort: (but)->
    t = this
    o = t.options
    elt = o.ele
    return if but and but.get "disabled"
    dirt_icon = but.getE ".dirt_icon"
    return if !dirt_icon
    sort_fld = but.get "h:sort_fld"
    return if !sort_fld
    but.set "disabled",true if but
    if dirt_icon.hasClass "asc_icon"
      srtObj = {sort_fld:sort_fld,dirt:"asc"}
      yield o.thisSrv.ajax "sortAdd",[srtObj]
    else if dirt_icon.hasClass "desc_icon"
      srtObj = {sort_fld:sort_fld,dirt:"desc"}
      yield o.thisSrv.ajax "sortAdd",[srtObj]
    else
      yield o.thisSrv.ajax "sortDel",[sort_fld]
    but.set "disabled",false if but
    return
  "@thClk":{comment:"点击表头,排序"}
  thClk: (but,e)->
    t = this
    o = t.options
    elt = o.ele
    return if but and but.get "disabled"
    dirt_icon = but.getE ".dirt_icon"
    return if !dirt_icon
    #由升序变为降序
    if dirt_icon.hasClass "asc_icon"
      dirt_icon.removeClass "asc_icon"
      dirt_icon.addClass "desc_icon"
      dirt_icon.show()
    #由降序变为无序
    else if dirt_icon.hasClass "desc_icon"
      dirt_icon.removeClass "desc_icon"
      dirt_icon.removeClass "asc_icon"
      dirt_icon.hide()
    #由无序变为升序
    else
      dirt_icon.removeClass "desc_icon"
      dirt_icon.addClass "asc_icon"
      dirt_icon.show()
    yield t.syncSort but
    #刷新表格
    yield t.firstPgClk undefined,undefined,true
    return
  "@initSearchList":{comment:"初始化表格搜索条件"}
  initSearchList:->
    t = this
    o = t.options
    elt = o.ele
    srch = elt.getE ".srch"
    return if !srch
    srch_but = srch.getE ".srch_but"
    srch_but.store "clickMethod",{t:t,method:t["srch_butClk"]} if t["srch_butClk"]
    srch_but.addEvent "click",(e)->
      co t.srch_butClk this,e
      return
    return
  closePage: ->
    t = this
    o = t.options
    yield SysWin.prototype.closePage.apply t,arguments
    return
  "@srch_butClk":{comment:"点击增加搜索按钮"}
  srch_butClk: (but,e)->
    t = this
    o = t.options
    elt = o.ele
    srch = elt.getE ".srch"
    srch_pop = srch.getE ".srch_pop"
    srch_pop = srch_pop.clone()
    srch_pop.set "tabindex",1
    srch_pop.addEvent "keydown",(e)->
      co t.srch_popKwn this,e
      return
    srch_cfm = srch_pop.getE ".srch_cfm"
    srch_name = srch_pop.getE ".srch_name"
    srch_name.addEvent "change",(e)->
      srch_pop.set "h:srch_name",this.get "value"
      co ->
        yield t.initSrch_opt srch_pop
        yield t.initSrch_value srch_pop
        return
      return
    srch_cfm.addEvent "click",(e)->
      co t.srch_cfmClk this,e,srch_pop
      return
    srch_pop.show()
    yield t.doModal srch_pop,"增加搜索条件"
    srch_pop.focus()
    return
  "@srch_popKwn":{comment:"Esc按键关闭搜索框,回车为确定"}
  srch_popKwn: (but,e)->
    t = this
    o = t.options
    code = e.code
    if [27,13].indexOf(code) isnt -1
      e.stop()
    if code is 27
      do_modal_div = but.getParent ".do_modal_div"
      do_modal_div.destroy()
    else if code is 13
      srch_cfm = but.getE ".srch_cfm"
      yield t.srch_cfmClk srch_cfm,undefined,but
    return
  "@srch_cfmClk": {comment:"点击确定后,增加搜索条件到表格上"}
  srch_cfmClk: (but,e,srch_pop)->
    t = this
    o = t.options
    do_modal_div = srch_pop.getParent ".do_modal_div"
    srch_name = srch_pop.getE ".srch_name"
    if srch_name.selectedIndex is 0 or srch_name.selectedIndex is -1
      do_modal_div.destroy()
      return
    srch_and = srch_pop.getE ".srch_and"
    srch_opt = srch_pop.getE ".srch_opt"
    if srch_opt.selectedIndex is -1
      do_modal_div.destroy()
      return
    srch_value = srch_pop.getE ".srch_value"
    if !srch_value
      do_modal_div.destroy()
      return
    andOr = srch_and.get "value"
    name = srch_name.get "value"
    opt = srch_opt.get "value"
    andOrLbl = srch_and.selectedOptions[0] and srch_and.selectedOptions[0].get "html"
    nameLbl = srch_name.selectedOptions[0] and srch_name.selectedOptions[0].get "html"
    optLbl = srch_opt.selectedOptions[0] and srch_opt.selectedOptions[0].get "html"
    srch_valueWg = srch_value.wg()
    value = ""
    if srch_valueWg
      value = srch_valueWg.getVal {cmpOldVal:false}
    else
      value = srch_value.get "value"
    valueLbl = t.srchValueLbl srch_value
    #搜索对象
    seaObj = {andOr:andOr,andOrLbl:andOrLbl,name:name,nameLbl:nameLbl,opt:opt,optLbl:optLbl,value:value,valueLbl:valueLbl}
    but.set "disabled",true if but
    yield t.srchAdd seaObj
    but.set "disabled",false if but
    do_modal_div.destroy()
    return
  srchValueLbl: (srch_value)->
    t = this
    o = t.options
    valueLbl = ""
    srch_valueWg = srch_value.wg()
    if srch_valueWg
      value = srch_valueWg.getVal {cmpOldVal:false}
    else
      value = srch_value.get "value"
    if srch_value.get("h:apply") is "Select"
      optArr = srch_value.getEs "option"
      optEl = optArr[srch_value.selectedIndex]
      valueLbl = optEl.get "html"
    else
      valueType = typeOf value
      if valueType is "string"
        valueLbl = value.truncate 4,".."
      else if valueType is "number"
        valueLbl = String value
      else if valueType is "boolean"
        valueLbl = ""
        if value
          valueLbl = "是"
        else
          valueLbl = "否"
    valueLbl
  "@srchAdd": {comment:"给前台后台增加搜索条件"}
  srchAdd: (seaObj)->
    t = this
    o = t.options
    elt = o.ele
    return if !seaObj or seaObj.value is undefined or seaObj.value is null
    srch_itemArrEl = elt.getE ".srch_itemArr"
    srch_itemArr = srch_itemArrEl.getEs ".srch_item"
    has = false
    for siTmp in srch_itemArr
      sob = siTmp.retrieve "seaObj"
      if seaObj.andOr is sob.andOr and seaObj.name is sob.name and seaObj.opt is sob.opt and seaObj.value is sob.value
        has = true
        break
    return if has
    yield o.thisSrv.ajax "srchAdd",[seaObj]
    srch_item = new Element "div.srch_item",{html:"""
    <div class="srch_item_andOr" title="#{seaObj.andOr}">#{seaObj.andOrLbl}</div><!--
    --><div class="srch_item_name" title="#{seaObj.name}">#{seaObj.nameLbl}</div><!--
    --><span class="srch_item_opt" title="#{seaObj.opt}">#{seaObj.optLbl}</span><!--
    --><div class="srch_item_value" title="#{seaObj.value}">#{seaObj.valueLbl}</div>
    """}
    srch_item.store "seaObj",seaObj
    sisLen = srch_itemArr.length
    if sisLen is 0
      srch_item.getE(".srch_item_andOr").hide()
    srch_item.addEvent "click",(e)->
      co t.srch_itemClk this,e
      return
    srch_item.inject srch_itemArrEl
    #刷新表格
    yield t.dataCount()
    yield t.firstPgClk undefined,undefined,true
    return
  "@srch_itemClk":{comment:"点击单个搜索条件,前后台删除搜索条件"}
  srch_itemClk: (but,e)->
    t = this
    o = t.options
    return if but and but.get "disabled"
    srch_itemArrEl = but.getParent ".srch_itemArr"
    seaObj = but.retrieve "seaObj"
    but.set "disabled",true if but
    yield o.thisSrv.ajax "srchDel",[seaObj]
    #刷新表格
    yield t.dataCount()
    yield t.firstPgClk undefined,undefined,true
    but.set "disabled",false if but
    but.destroy()
    #隐藏第一个搜索条件的andOr控件
    srch_item = srch_itemArrEl.getFirst ".srch_item"
    if srch_item
      srch_item_andOr = srch_item.getE ".srch_item_andOr"
      srch_item_andOr.hide()
    return
  "@initSrch_opt":{comment:"初始化搜索条件"}
  initSrch_opt: (srch_pop)->
    t = this
    o = t.options
    srch_name = srch_pop.getE ".srch_name"
    srch_opt = srch_pop.getE ".srch_opt"
    srch_opt.empty()
    srch_value_div = srch_pop.getE ".srch_value_div"
    optArr = srch_name.getEs "option"
    optEl = optArr[srch_name.selectedIndex]
    return if !optEl
    h_type = optEl.get "h:type"
    option = new Element "option",{value:"=",text:"等于"}
    option.inject srch_opt
    if h_type is "Number" or h_type is "DateTime" or h_type is "Date" or h_type is "Time"
      option = new Element "option",{value:">",text:"大于"}
      option.inject srch_opt
      option = new Element "option",{value:">=",text:"大于等于"}
      option.inject srch_opt
      option = new Element "option",{value:"<",text:"小于"}
      option.inject srch_opt
      option = new Element "option",{value:"<=",text:"小于等于"}
      option.inject srch_opt
    else if h_type is "TextInput"
      option = new Element "option",{value:"begin",text:"始于"}
      option.inject srch_opt
      option = new Element "option",{value:"end",text:"止于"}
      option.inject srch_opt
      option = new Element "option",{value:"like",text:"包含"}
      option.inject srch_opt
    return
  "@srchOpt2Iez":{comment:"给initSrch_value调用,根据下拉框的选项创建搜索控件"}
  srchOpt2Iez: (optEl)->
    t = this
    o = t.options
    return if !optEl
    iez = undefined
    h_type = optEl.get "h:type"
    return if !optEl
    if h_type is "TextInput"
      iez = new Element "input",{type:"text","h:apply":"TextInput"}
    else if h_type is "Date"
      iez = new Element "input",{type:"datetime-local","h:apply":"Date"}
    else if h_type is "Checkbox"
      iez = new Element "input",{type:"checkbox","h:apply":"Checkbox"}
    else if h_type is "Number"
      iez = new Element "input",{type:"number","h:apply":"Number"}
    else
      iez = new Element "input",{type:"text","h:apply":"TextInput"}
    iez
  "@initSrch_value":{comment:"初始化搜索的控件类型"}
  initSrch_value: (srch_pop)->
    t = this
    o = t.options
    srch_name = srch_pop.getE ".srch_name"
    srch_value_div = srch_pop.getE ".srch_value_div"
    srch_value_div.empty()
    optArr = srch_name.getEs "option"
    optEl = optArr[srch_name.selectedIndex]
    iez = t.srchOpt2Iez optEl
    iez.addClass "srch_value"
    iez.inject srch_value_div
    yield iez.onDrawAsync()
    return
  initGrid: ->
    t = this
    o = t.options
    elt = o.ele
    t.initButEvt ["addPg","firstPg","prevPg","nextPg","lastPg","refresh"]
    yield t.dataCount()
    yield t.firstPgClk undefined,undefined,true
    cur_pg = elt.getE ".cur_pg"
    cur_pg.addEvent "change",->
      co t.cur_pgChg()
      return
    cur_pg.addEvent "keydown",(e)->
      code = e.code
      if code is 13
        co t.cur_pgChg()
      return
    pgNumSlt = elt.getE ".pgNumSlt"
    pgNumSlt.addEvent "change",->
      co t.pgNumSltChg this
      return
    return
  "@pgNumSltChg":{comment:"每页显示多少行"}
  pgNumSltChg: (but)->
    t = this
    o = t.options
    elt = o.ele
    but.set "disabled",true if but
    yield t.dataCount()
    yield t.firstPgClk undefined,undefined,true
    but.set "disabled",false if but
    return
  "@refreshClk":{comment:"点击刷新按钮"}
  refreshClk: (but)->
    t = this
    yield t.cur_pgChg true
    return
  "@cur_pgChg":{comment:"""
    跳转到当前页码对应的数据
      isRef 如果为true,则强制刷新页面
  """}
  cur_pgChg: (isRef)->
    t = this
    o = t.options
    elt = o.ele
    cur_pg = elt.getE ".cur_pg"
    pdObj = t.getPgOftNum isRef
    #记录偏移量,每页显示记录数
    pgOffset = pdObj.pgOffset
    pgNum = pdObj.pgNum
    return if cur_pg.get("value") is cur_pg.get("h:old_value") and isRef isnt true
    yield t.dataGrid pgOffset,pgNum
    cur_pg.set "h:old_value",cur_pg.get "value"
    return
  firstPgClk: (but,e,isRef)->
    t = this
    o = t.options
    elt = o.ele
    but = elt.getE "[h:but='firstPg']" if !but
    cur_pg = elt.getE ".cur_pg"
    pgCur = cur_pg.get("value").toInt()
    cur_pg.set "value",1
    but.set "disabled",true
    yield t.cur_pgChg isRef
    but.set "disabled",false
    return
  nextPgClk: (but)->
    t = this
    o = t.options
    elt = o.ele
    cur_pg = elt.getE ".cur_pg"
    pgCur = cur_pg.get("value").toInt()
    cur_pg.set "value",pgCur+1
    but.set "disabled",true if but
    yield t.cur_pgChg()
    but.set "disabled",false if but
    return
  prevPgClk: (but)->
    t = this
    o = t.options
    elt = o.ele
    cur_pg = elt.getE ".cur_pg"
    pgCur = cur_pg.get("value").toInt()
    cur_pg.set "value",pgCur-1
    but.set "disabled",true if but
    yield t.cur_pgChg()
    but.set "disabled",false if but
    return
  lastPgClk: (but)->
    t = this
    o = t.options
    elt = o.ele
    cur_pg = elt.getE ".cur_pg"
    #总页面数
    ttl_pg = elt.getE ".ttl_pg"
    pgCur = cur_pg.get("value").toInt()
    pgTtl = ttl_pg.get("text").toInt()
    cur_pg.set "value",pgTtl
    but.set "disabled",true if but
    yield t.cur_pgChg()
    but.set "disabled",false if but
    return
  #给dataCount调用,返回总记录数
  rltSetCount: ->
    t = this
    o = t.options
    rltSet = yield o.thisSrv.ajax "dataCount"
    rltSet
  dataCount: ->
    t = this
    o = t.options
    elt = o.ele
    ttl_pg = elt.getE ".ttl_pg"
    rltSet = yield t.rltSetCount()
    pdObj = t.getPgOftNum()
    #记录偏移量,每页显示记录数
    pgOffset = pdObj.pgOffset
    pgNum = pdObj.pgNum
    pgTtl = Math.ceil rltSet/pgNum
    pgTtl = 1 if pgTtl is 0
    ttl_pg.set "text",pgTtl
    return
  #删除一行
  optDelClk: (but)->
    t = this
    o = t.options
    elt = o.ele
    return if !window.confirm "确定删除?"
    tr = but.getParent ".dt_tr"
    eny = tr.retrieve "eny"
    but.set "disabled",true if but
    rltSet = yield t.delById eny.id
    window.ncWg.addNotice "info","删除 "+rltSet.rowCount+" 行记录!",2
    yield t.dataCount()
    yield t.cur_pgChg true
    but.set "disabled",false if but
    return
  delById: (id)->
    t = this
    o = t.options
    rltSet = yield o.thisSrv.ajax "delById",[id]
    rltSet
  #打开编辑页面
  optEditClk: (but)->
    t = this
    o = t.options
    elt = o.ele
    tr = but.getParent ".dt_tr"
    eny = tr.retrieve "eny"
    bodyWg = $(document.body).wg()
    pageEny = 
      url: "/web/sys/MenuEdit.html"
      enable: true
    mldOpt = 
      width:600
      height:550
      befOnDraw: (win)->
        win.store "eny",eny
        return
    win = yield bodyWg.menuPg pageEny,"modal",mldOpt
    winWg = win.wg()
    aftSave = winWg.aftSave
    winWg.aftSave = ->
      yield t.dataCount()
      yield t.cur_pgChg true
      rv = yield aftSave.apply this,arguments
      rv
    win
  "@addPgClk":{comment:"打开增加页面,重写保存成功之后的方法,保存成功之后,刷新表格"}
  addPgClk: (but)->
    t = this
    o = t.options
    elt = o.ele
    bodyWg = $(document.body).wg()
    pageEny = 
      lbl: "菜单增加"
      url: "/web/sys/MenuAdd.html"
      enable: true
    but.set "disabled",true if but
    win = yield bodyWg.menuPg pageEny,"modal",{width:600,height:550}
    but.set "disabled",false if but
    winWg = win.wg()
    aftSave = winWg.aftSave
    winWg.aftSave = ->
      yield t.dataCount()
      yield t.cur_pgChg true
      rv = yield aftSave.apply this,arguments
      rv
    win
  "@getPgOftNum":{comment:"获得表格分页的偏移量pgOffset跟每页显示记录数pgNum"}
  getPgOftNum: ->
    t = this
    o = t.options
    elt = o.ele
    cur_pg = elt.getE ".cur_pg"
    ttl_pg = elt.getE ".ttl_pg"
    pgNumSlt = elt.getE ".pgNumSlt"
    pgTtl = ttl_pg.get("text").toInt()
    pgTtl = 1 if pgTtl is 0
    pgCur = cur_pg.get("value").toInt()
    if isNaN pgCur
      cur_pg.set "value",cur_pg.get "h:old_value"
      return
    if pgCur < 1
      pgCur = 1
      cur_pg.set "value",pgCur
    if pgCur > pgTtl
      pgCur = pgTtl
      cur_pg.set "value",pgCur
    pgNum = Number pgNumSlt.get "value"
    pgOffset = (pgCur-1)*pgNum
    {pgOffset:pgOffset,pgNum:pgNum}
  #给dataGrid调用
  rltSetGrid: (pgOffset,pgNum)->
    t = this
    o = t.options
    rltSet = yield o.thisSrv.ajax "dataGrid",[pgOffset,pgNum]
    rltSet
  dataGrid: (pgOffset,pgNum)->
    t = this
    o = t.options
    elt = o.ele
    grid_tbl = elt.getE ".grid_tbl"
    tbody = grid_tbl.getFirst "tbody"
    #获取数据
    rltSet = yield t.rltSetGrid pgOffset,pgNum
    tbody.empty()
    for i in [0...rltSet.length]
      eny = rltSet[i]
      t.initTr eny,tbody
    return
  "@trClk":{comment:"点击表格中的一行"}
  trClk: (tr)->
    t = this
    o = t.options
    elt = o.ele
    yield t.trSld tr
    return
  "@trSld":{comment:"选中表格中的一行"}
  trSld: (tr)->
    t = this
    o = t.options
    elt = o.ele
    tbody = tr.getParent()
    trArr = tbody.getEs ".dt_tr"
    for trTmp in trArr
      if trTmp is tr
        trTmp.addClass "dt_tr_sld"
      else
        trTmp.removeClass "dt_tr_sld"
    return
  "@initTr":{comment:"初始化表格中的一行,dataGrid->initTr"}
  initTr: (eny,tbody)->
    t = this
    o = t.options
    elt = o.ele
    tr = new Element "tr.dt_tr"
    tr.store "eny",eny
    tr.inject tbody
    tr.addEvent "click",->
      co t.trClk this
      return
    
    t.initSltTd tr
    t.initOptTd tr
    
    for j in [0...o.headArr.length]
      key = o.headArr[j]
      t.initTd tr,key
    tr.inject tbody if tbody
    tr
  "@initSltTd":{comment:"选择"}
  initSltTd: (tr)->
    t = this
    sltTd = new Element "td.sltTd"
    sltTd.inject tr
    checkbox = new Element "input[type=checkbox].sltCbx"
    checkbox.inject sltTd
    return
  "@initOptTd":{comment:"操作"}
  initOptTd: (tr)->
    t = this
    optTd = new Element "td.optTd"
    optTd.inject tr
    t.initOptDel optTd
    t.initOptEdit optTd
    return
  "@initOptEdit":{comment:"初始化修改按钮"}
  initOptEdit: (optTd)->
    t = this
    o = t.options
    elt = o.ele
    optEdit = new Element "button",{"class":"optEdit",title:"修改"}
    optEdit.addEvent "click",(e)->
      e.stop()
      co t.optEditClk this
      return
    optEdit.inject optTd
    optEditImg = new Element "img",{"class":"optEditImg",src:"/web/img/edit.png"}
    optEdit.addEvents {
      mouseenter: ->
        this.getEs(".optEditImg").set "src","/web/img/edit_light.png"
        return
      mouseleave: ->
        this.getEs(".optEditImg").set "src","/web/img/edit.png"
        return
    }
    optEditImg.inject optEdit
    return
  "@initOptDel":{comment:"初始化删除按钮"}
  initOptDel: (optTd)->
    t = this
    o = t.options
    elt = o.ele
    optDel = new Element "button",{"class":"optDel",title:"删除"}
    optDel.addEvent "click",(e)->
      e.stop()
      co t.optDelClk this
      return
    optDel.inject optTd
    optDelImg = new Element "img",{"class":"optDelImg",src:"/web/img/delete.png"}
    optDel.addEvents {
      mouseenter: ->
        this.getEs(".optDelImg").set "src","/web/img/delete_light.png"
        return
      mouseleave: ->
        this.getEs(".optDelImg").set "src","/web/img/delete.png"
        return
    }
    optDelImg.inject optDel
    return
  initTd: (tr,key)->
    td = new Element "td"
    td.inject tr
    eny = tr.retrieve "eny"
    val = eny[key]
    typ = typeOf val
    if typ is "object" or typ is "array"
      val = JSON.encode val
      td.set "text",val
    else if typ is "boolean"
      if val
        td.set "text","是"
      else
        td.set "text","否"
    else
      td.set "text",val
    td.addClass "initTd_"+key
    return
