require "./EmpList.css"
{SysList} = require "./SysList"

exports["@EmpList"] = {comment:"员工列表"}
exports.EmpList = new Class
  Extends: SysList
  options:
    "$id":"EmpList"
    enyStr: "Emp"
    headArr: ["code","name","username","d.lbl","r.lbl","sex","addr","wtel","htel","mtel","birthdate","origin","com_addr","zip","card","nation","religiou","edu_lvl","school","politic","occup","job","position","interest","skill","marital","health","fax","email","qq","msn","person","introduction","picture","enable"]
    headObj:
      "code":"编码"
      "name":"姓名"
      "username":"用户名"
      "d.lbl":"部门"
      "r.lbl":"角色"
      "sex":"性别"
      "addr":"家庭地址"
      "wtel":"办公电话"
      "htel":"家庭电话"
      "mtel":"移动电话"
      "birthdate":"出生日期"
      "origin":"籍贯"
      "com_addr":"通信地址"
      "zip":"邮编"
      "card":"身份证号"
      "nation":"民族"
      "religiou":"宗教信仰"
      "edu_lvl":"教育程度"
      "school":"毕业学校"
      "politic":"政治面貌"
      "occup":"职业类型"
      "job":"职务"
      "position":"职位"
      "interest":"爱好"
      "skill":"特长"
      "marital":"婚姻状况"
      "health":"健康状况"
      "fax":"传真"
      "email":"邮箱"
      "qq":"QQ"
      "msn":"MSN"
      "person":"性格描述"
      "introduction":"个人简述"
      "picture":"照片"
      "enable":"启用"
  initPg: ->
    t = this
    yield SysList.prototype.initPg.apply t,arguments
    return
