require "./Login.css"
{SysWin} = require "./SysWin"
{Srv} = require "Srv"

exports["@Login"] = {comment:"登录"}
exports.Login = new Class
  Extends: SysWin
  options:
    thisSrv: undefined
    butObj: {}
    headArr: ["code","password"]
  initThisSrv: ->
    t = this
    o = t.options
    elt = o.ele
    o.thisSrv = new Srv(clz:"sys.LoginSrv")
    o.thisSrv.options.ele = elt
    return
  onDraw: ->
    t = this
    o = t.options
    t.initThisSrv()
    t.initButEvt ["commit"]
    ###debug yield t.setPgVal {code:"admin",password:"admin123"}
    commit = $(t).getE "[h:but=commit]"
    commit.click() ###
    return
  commitClk: (but)->
    t = this
    o = t.options
    elt = o.ele
    eny = yield t.getPgVal()
    if String.isEmpty eny.code
      window.ncWg.addNotice "error","用户名不能为空!",2
      codeEl = elt.getE "[h:iez=code]"
      codeEl.focus()
      codeEl.selectRange()
      return
    if !eny.password
      window.ncWg.addNotice "error","密码不能为空!",2
      passwordEl = elt.getE "[h:iez=password]"
      passwordEl.focus()
      passwordEl.selectRange()
      return
    but.set "disabled",true if but
    yield o.thisSrv.ajax "_sessionId"
    rss = yield o.thisSrv.ajax "login",[eny.code,eny.password]
    but.set "disabled",false if but
    errMsg = rss.errMsg
    if errMsg
      window.ncWg.addNotice "error",errMsg,4
    if rss.suc is true
      yield t.lgnSucc()
    return
