require "./SysWin.css"
{Component} = require "Component"

exports["@SysWin"] = {comment:"主窗口基类"}
exports.SysWin = new Class
  Extends: Component
  onDraw: ->
    t = this
    o = t.options
    t.initAvalon()
    t.initThisSrv()
    yield t.initPg()
    return
  initAvalon: ->
    t = this
    o = t.options
    elt = o.ele
    if o["$id"]
      t.options = avalon.define t.options
      elt.set "ms-controller",o["$id"]
      avalon.scan elt
    return
  initThisSrv: ->
  "@sttConfirm":{comment:"会话超时,由后台的action调用"}
  sttConfirm: ->
    t = this
    o = t.options
    elt = o.ele
    stwin = $(document.body).getE ".session_timeout_win"
    stwin.getParent(".do_modal_div").show()
    stwin.getElement(".confirm_ok").focus()
    return
  #获得包含此页面的选项卡
  getLinkTab: ->
    t = this
    o = t.options
    elt = o.ele
    tabpanel = elt.getParent ".tabpanel"
    return null if !tabpanel
    tabpanelWg = tabpanel.wg()
    tabpanelWg.getLinkTab()
  getTabWg: ->
    t = this
    tab = t.getLinkTab()
    return if !tab
    tab.wg()
  "@getPgVal":{comment:"获得页面上控件h:iez的值,当值没有被修改时,会返回undefined"}
  getPgVal: (eny,headArr)->
    t = this
    o = t.options
    elt = o.ele
    eny = eny or {}
    headArr = headArr or o.headArr
    for key in headArr
      iez = elt.getE "[h:iez="+key+"]"
      continue if !iez
      iezWg = iez.wg()
      continue if !iezWg
      val = yield iezWg.getVal()
      eny[key] = val if val isnt undefined
    eny
  "@setPgVal":{comment:"设置页面上控件h:iez的值,当传入undefined时,自动使用默认值oldValue"}
  setPgVal: (eny,headArr)->
    t = this
    o = t.options
    elt = o.ele
    eny = eny or {}
    headArr = headArr or o.headArr
    for key in headArr
      iez = elt.getE "[h:iez="+key+"]"
      continue if !iez
      iezWg = iez.wg()
      continue if !iezWg
      val = eny[key]
      yield iezWg.setVal val
    return
  setPgOldVal: (eny,headArr)->
    t = this
    o = t.options
    elt = o.ele
    eny = eny or {}
    headArr = headArr or o.headArr
    for key in headArr
      iez = elt.getE "[h:iez="+key+"]"
      continue if !iez
      iezWg = iez.wg()
      continue if !iezWg
      val = eny[key]
      iezWg.setOldVal val
    return
  closePage: ->
    t = this
    o = t.options
    if o["$id"]
      i = avalon.$$subscribers.length
      while obj = avalon.$$subscribers[--i]
        if o.ele.contains obj.data.element
          avalon.$$subscribers.splice i, 1
          delete avalon.$$subscribers[obj.$$uuid]
          avalon.Array.remove obj.list, obj.data
      delete avalon.vmodels[o["$id"]]
    yield o.thisSrv.closeSrv() if o.thisSrv
    return
  initButEvt: (butArr,sldArr)->
    t = this
    o = t.options
    elt = o.ele
    butArr.each (item,i)->
       sldStr = sldArr and sldArr[i] or "[h:but="+item+"]"
       but = elt.getE sldStr
       if !but
         #alert item+" h:but dose not exist!"
         return
       but.store "clickMethod",{t:t,method:t[item+"Clk"]} if t[item+"Clk"]
       but.addEvent "click",(e)->
         if !t[item+"Clk"]
           alert item+"Clk"
           return
         co t[item+"Clk"] this,e
         return
      return
    return
  #把窗口变成模态窗口
  doModal: (winWg,lbl,mdlOpt)->
    height = mdlOpt and mdlOpt.height
    width = mdlOpt and mdlOpt.width
    hideCloseBut = mdlOpt and mdlOpt.hideCloseBut
    befOnDraw = mdlOpt and mdlOpt.befOnDraw
    lbl = lbl or ""
    body = $ window.document.body
    oldWinWg = winWg
    do_modal_div = new Element "div",{"class":"do_modal_div"}
    do_modal_titlebar = new Element "div",{"class":"do_modal_titlebar",html: "<div class='do_modal_title'>"+lbl+"</div>"+"<div class='do_modal_close_but'>X</div>"+"<div class='clear'></div>"}
    do_modal_win = new Element "div",{"class":"do_modal_win"}
    do_modal_titlebar.inject do_modal_win
    do_modal_win.inject do_modal_div
    do_modal_win.grab $ winWg
    do_modal_div.inject body,"bottom"
    if typeOf(winWg) is "element"
      yield befOnDraw winWg if befOnDraw
      yield winWg.onDrawAsync()
      winWg = winWg.retrieve "widget"
    closeBut = do_modal_titlebar.getE ".do_modal_close_but"
    do_modal_titlebar.addEvent "dblclick",->
      if closeBut.getStyle("display") isnt "none"
        if mdlOpt and mdlOpt.closeFn
          co mdlOpt.closeFn winWg or oldWinWg
        else
          if winWg
            co winWg.closePage()
          else
            do_modal_div.destroy()
      return
    closeBut.hide() if hideCloseBut is true
    if winWg
      closeBut and closeBut.addEvent "click",->
        if mdlOpt and mdlOpt.closeFn
          co mdlOpt.closeFn winWg or oldWinWg
        else
          co winWg.closePage()
        return
    else
      closeBut and closeBut.addEvent "click",->
        if mdlOpt and mdlOpt.closeFn
          co mdlOpt.closeFn winWg or oldWinWg
        else
          do_modal_div.destroy()
        return
    $(oldWinWg).setStyle "height",height if height
    $(oldWinWg).setStyle "width",width if width
    new Drag do_modal_win,{handle:do_modal_titlebar,modifiers:{x:'left',y:'top'}}
    left = 0
    top = 0
    bdSz = do_modal_div.measure -> this.getSize()
    winSz = do_modal_win.measure -> this.getSize()
    left = (bdSz.x-winSz.x)/2
    top = (bdSz.y-winSz.y)/3
    do_modal_win.setStyles {left:left,top:top,"max-width":bdSz.x,"max-height":bdSz.y}
    do_modal_div
