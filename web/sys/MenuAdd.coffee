{SysAdd} = require "./SysAdd"
{Srv} = require "Srv"
{Tree} = require "Tree"

exports["@MenuAdd"] = {comment:"菜单增加"}
exports.MenuAdd = new Class
  Extends: SysAdd
  options:
    $id: "MenuAdd"
    enyStr: "Menu"
    headArr: ["prn_id","lbl","page_id","open_op","is_root","is_leaf","enable","sort_num"]
    headObj:
      "prn_id":"父菜单"
      "lbl":"标签"
      "page_id":"页面"
      "open_op":"打开方式"
      "is_root":"根节点"
      "is_leaf":"叶子"
      "enable":"生效"
      "sort_num":"排序"
  initPg: ->
    t = this
    o = t.options
    elt = o.ele
    yield t.initPrn_id()
    yield t.initPage_id()
    return
  confirmRst: (eny,headArr)->
    t = this
    o = t.options
    headArr.erase "prn_id"
    rltSet = yield SysAdd.prototype.confirmRst.apply t,[eny,headArr]
    rltSet
  initPrn_id: ->
    t = this
    o = t.options
    elt = o.ele
    prn_id = elt.getE "[h:iez=prn_id]"
    cb_input = elt.getE ".combotree_input"
    cb_tree = prn_id.getE ".combotree_tree"
    cb_treeWg = cb_tree.wg()
    setVal = cb_treeWg.setVal
    cb_treeWg.setVal = (id)-> yield setVal.apply this,[{id:id}]
    getVal = cb_treeWg.getVal
    cb_treeWg.getVal = ->
      eny = yield getVal.apply this,arguments
      return 0 if eny is null
      if eny
        oldVal = this.getOldVal()
        return if oldVal is eny.id
        return eny.id
      return
    cb_treeWg.treAllPrnId = (eny)->
      return [] if !eny
      rltSet = yield o.thisSrv.ajax "treAllPrnId",[eny.id]
      rltSet
    cb_treeWg.getLbl = (tree_li)->
      eny = tree_li.retrieve "eny"
      lbl = eny and eny.lbl or ""
      lbl
    cb_treeWg.isLeaf = (tree_li)->
      eny = tree_li.retrieve "eny"
      return eny.is_leaf if eny
      false
    getChildren = cb_treeWg.getChildren
    cb_treeWg.getChildren = (tree_li,hLvl)->
      liElArr = yield getChildren.apply this,arguments
      rltSet = []
      if hLvl is 0
        rltSet = yield o.thisSrv.ajax "menuRoot"
      else
        eny = tree_li.retrieve "eny"
        rltSet = yield o.thisSrv.ajax "menuCld",[eny.id]
      for eny in rltSet
        li = new Element "div"
        li.store "eny",eny
        liElArr.push li
      liElArr
    yield cb_treeWg.dataTree()
    return
  "@frtElFcu":{comment:"first element focus 页面打开后第一个控件获得焦点"}
  frtElFcu: ->
    t = this
    o = t.options
    elt = o.ele
    lblEl = elt.getE "[h:iez=lbl]"
    lblEl.focus()
    return
  initPage_id: ->
    t = this
    o = t.options
    elt = o.ele
    page_id = elt.getE "[h:iez=page_id]"
    cb_input = elt.getE ".combotree_input"
    cb_tree = page_id.getE ".combotree_tree"
    cb_treeWg = cb_tree.wg()
    setVal = cb_treeWg.setVal
    cb_treeWg.setVal = (id)->
      menuEny = yield o.thisSrv.ajax "findMenuByPageId",[id]
      yield setVal.apply this,[menuEny]
    getVal = cb_treeWg.getVal
    cb_treeWg.getVal = ->
      eny = yield getVal.apply this,arguments
      return 0 if eny is null
      if eny
        oldVal = this.getOldVal()
        return if oldVal is eny.page_id
        return eny.page_id
      return
    selectLi = cb_treeWg.selectLi
    cb_treeWg.selectLi = (tree_li)->
      isLeaf = yield this.isLeaf tree_li
      return if !isLeaf
      yield selectLi.apply this,arguments
    cb_treeWg.liAddEvt = -> Tree.prototype.liAddEvt.apply this,arguments
    cb_treeWg.treAllPrnId = (eny)->
      return [] if !eny
      rltSet = yield o.thisSrv.ajax "treAllPrnId",[eny.id]
      rltSet
    cb_treeWg.getLbl = (tree_li)->
      eny = tree_li.retrieve "eny"
      lbl = eny and eny.lbl or ""
      lbl
    cb_treeWg.isLeaf = (tree_li)->
      eny = tree_li.retrieve "eny"
      return eny.is_leaf if eny
      true
    getChildren = cb_treeWg.getChildren
    cb_treeWg.getChildren = (tree_li,hLvl)->
      liElArr = yield getChildren.apply this,arguments
      rltSet = []
      if hLvl is 0
        rltSet = yield o.thisSrv.ajax "pageRoot"
      else
        eny = tree_li.retrieve "eny"
        rltSet = yield o.thisSrv.ajax "pageCld",[eny.id] if eny
      for eny in rltSet
        li = new Element "div"
        li.store "eny",eny
        liElArr.push li
      liElArr
    yield cb_treeWg.dataTree()
    return
