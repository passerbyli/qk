require "./SysAdd.css"
{SysWin} = require "SysWin"
{Srv} = require "Srv"

exports["@SysAdd"] = {comment:"增加页面基类"}
exports.SysAdd = new Class(
  Extends: SysWin
  options:
    enyStr: ""
    headArr: []
  onDraw: ->
    t = this
    o = t.options
    t.initAvalon()
    t.initThisSrv()
    yield t.initPg()
    t.initButEvt ["cancelBut","confirmBut"]
    t.initPgEvt()
    return
  initPg: ->
  initThisSrv: ->
    t = this
    o = t.options
    elt = o.ele
    o.thisSrv = new Srv clz:"sys."+o.enyStr+"AddSrv"
    o.thisSrv.options.ele = elt
    return
  "@initPgEvt":{comment:"初始化页面按钮事件,例如ctrl+s 保存"}
  initPgEvt: ->
    t = this
    o = t.options
    elt = o.ele
    elt.addEvent "keydown",(e)->
      code = e.code
      if e.control and code is 83
        e.stop()
        confirmBut = elt.getE ".confirmBut"
        co t.confirmButClk confirmBut
      else if code is 27
        e.stop()
        cancelBut = elt.getE ".cancelBut"
        co t.cancelButClk cancelBut
      return
    return
  "@aftDoModal":{comment:"模态窗口显示之后调用此方法"}
  aftDoModal: ->
    t = this
    t.frtElFcu()
    return
  "@frtElFcu":{comment:"first element focus 页面打开后第一个控件获得焦点"}
  frtElFcu: ->
    t = this
    o = t.options
    elt = o.ele
    lblEl = elt.getE "[h:iez]"
    lblEl.focus()
    return
  confirmButClk: (but,e,headArr)->
    t = this
    o = t.options
    elt = o.ele
    eny = {}
    headArr = headArr or o.headArr
    yield t.getPgVal eny,headArr
    but.set "disabled",true if but
    rltSet = yield t.confirmRst eny,headArr
    but.set "disabled",false if but
    yield t.aftSave rltSet
    return
  confirmRst: (eny,headArr)->
    t = this
    o = t.options
    rltSet = yield o.thisSrv.ajax "confirmButClk",[eny,headArr]
    rltSet
  "@aftSave": {comment:"保存成功之后,关闭页面"}
  aftSave: (rltSet)->
    t = this
    o = t.options
    elt = o.ele
    if rltSet
      window.ncWg.addNotice "info","保存成功!",2
      cancelBut = elt.getE ".cancelBut"
      yield t.cancelButClk cancelBut,undefined,false
    return
  cancelButClk: (but,e,isCkd)->
    t = this
    o = t.options
    elt = o.ele
    if isCkd isnt false
      eny = yield t.getPgVal()
      if Object.keys(eny).length isnt 0
        return if !window.confirm "数据尚未保存!是否继续退出!"
    do_modal_div = elt.getParent ".do_modal_div"
    but.set "disabled",true if but
    yield t.closePage()
    but.set "disabled",false if but
    do_modal_div.destroy()
    return
  closePage: ->
    t = this
    o = t.options
    yield SysWin.prototype.closePage.apply t,arguments
    return
)