{SysWin} = require "SysWin"
{Srv} = require "Srv"
exports["@MainFrame"] = {comment:"主页面"}
exports.MainFrame = new Class
  Extends: SysWin
  options:
    thisSrv: undefined
  initThisSrv: ->
    t = this
    o = t.options
    elt = o.ele
    o.thisSrv = new Srv clz:"sys.MainFrameSrv"
    o.thisSrv.options.ele = elt
    return
  onDraw: ->
    t = this
    o = t.options
    t.initThisSrv()
    yield t.initLogin()
    ###debug $(document).addEvent "keydown",(e)->
      code = e.code
      uid = 'b2730998-8286-469c-ac16-97757ff76956'
      if code is 117
        co(->
          try
            rltSet = yield o.thisSrv.ajax "_clearCache",[uid]
            window.ncWg.addNotice "info",rltSet,1
          catch err
            console.error err
          return
        )
        e.stop()
      else if code is 118
        for key of seajs.cache
          delete seajs.cache[key]
        for key of seajs.data.fetchedList
          delete seajs.data.fetchedList[key]
        window.ncWg.addNotice "info","js清空",1
      else if code is 119
        links = $$ "link"
        for link in links
          href = link.get "href"
          link.set "href",href
      return###
    return
  initLogin: ->
    t = this
    o = t.options
    elt = o.ele
    loginWin = yield Srv.createWindow "/web/sys/Login.html"
    loginWin = loginWin[0]
    loginWin.inject elt
    yield loginWin.onDrawAsync()
    loginWinWg = loginWin.wg()
    loginWinWg.lgnSucc = ->
      yield this.closePage()
      elt.getE(".main_bl_layout").setStyle "visibility",undefined
      loginWin.destroy()
      loginWin = undefined
      yield t.initFrm()
      return
    return
  "@menuPg":{comment:"""
    通过页面实体类打开页面
      @pageEny 表 page 的实体类
      @open_op 表 menu.open_op 打开页面的方式 tab选项卡, module模态窗口
      @opt 选项,高度宽度等,例如: {width:600,height:550}
      @return 返回被打开的页面win
  """}
  menuPg: (pageEny,open_op,opt)->
    t = this
    o = t.options
    elt = o.ele
    befOnDraw = opt and opt.befOnDraw
    page = pageEny.lbl
    win = elt.getE ".main_tabbox>.tabpanels>.tabpanel>[h:pg="+page+"]"
    if win
      winWg = win.wg()
      tabWg = winWg.getTabWg()
      tabWg.select() if tabWg
      return win
    url = pageEny.url
    return if String.isEmpty url
    lbl = lbl or page
    win = yield Srv.createWindow url
    win = win[0]
    win.set "h:pg",page
    win.store "pageEny",pageEny
    yield befOnDraw win if befOnDraw
    yield win.onDrawAsync()
    winWg = win.wg()
    if open_op is "tab"
      main_tabbox = elt.getE ".main_tabbox"
      main_tabboxWg = main_tabbox.wg()
      main_tabboxWg.hasNotTab = ->
      tabObj = yield main_tabboxWg.addTabAsync lbl,true
      tabObj.tabpanel.grab win
      tabWg = tabObj.tab.wg()
      tabWg.select()
      tmpFn = ->
        yield winWg.closePage()
        tabWg.deleteTab()
        tabWg = undefined
        winWg = undefined
        win = undefined
        return
      tabWg.addEvent "close",->
        co tmpFn()
        return
      tabObj.tab.addEvent "dblclick",->
        co tmpFn()
        return
    else if open_op is "modal"
      do_modal_div = yield t.doModal winWg,lbl,opt
      yield winWg.aftDoModal() if winWg.aftDoModal
      closePage = winWg.closePage
      winWg.closePage = ->
        rv = yield closePage.apply this,arguments
        do_modal_div.destroy()
        do_modal_div = undefined
        winWg = undefined
        win = undefined
        rv
    win
  "@menuPg":{comment:"通过菜单实体类打开页面"}
  menuClk: (menuEny,opt)->
    t = this
    o = t.options
    elt = o.ele
    open_op = menuEny.open_op or "tab"
    lbl = menuEny.lbl
    page_id = menuEny.page_id
    return if !page_id
    pageEny = yield o.thisSrv.ajax "findById",[page_id,"page"]
    return if !pageEny
    win = yield t.menuPg pageEny,open_op,opt
    win.store "menuEny",menuEny
    win
  initFrm: ->
    t = this
    o = t.options
    elt = o.ele
    menu_tree = elt.getE ".menu_tree"
    menu_treeWg = menu_tree.wg()
    menu_treeWg.treAllPrnId = (eny)->
      rltSet = yield o.thisSrv.ajax "treAllPrnId",[eny.id,"menu"]
      rltSet
    menu_treeWg.getLbl = (tree_li)->
      menuEny = tree_li.retrieve "eny"
      lbl = menuEny.lbl or ""
      lbl
    menu_treeWg.isLeaf = (tree_li)->
      menuEny = tree_li.retrieve "eny"
      menuEny.is_leaf
    menu_treeWg.getChildren = (tree_li,hLvl)->
      lis = []
      rltSet = []
      if hLvl is 0
        rltSet = yield o.thisSrv.ajax "menuRoot"
      else
        eny = tree_li.retrieve "eny"
        rltSet = yield o.thisSrv.ajax "menuCld",[eny.id]
      for eny in rltSet
        li = new Element "div"
        li.store "eny",eny
        li.addEvent "click",->
          co t.menuClk this.retrieve "eny"
          return
        lis.push li
      lis
    yield menu_treeWg.dataTree()
    return
