if(window.setImmediate === undefined) {
	window.setImmediate = function(callback){
		setTimeout(callback,0);
	};
}