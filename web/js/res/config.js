//全局配置信息
ops = {
	debug: true,
	//sessionId存储的方式,js,header,cookie
	sessionId: "header",
	//国际化缓存
	//,i18n:{}
	//sessionId
	//,_sid: ""
};
seajs.config({
	//模块映射
	alias: {
		Component: "/web/js/comp/Component",
		Srv: "/web/js/comp/Srv",
		//Bl_layout
		Bl_layout: "/web/js/comp/Bl_layout",
		//Tabbox
		Tabbox: "/web/js/comp/Bl_layout",
		Tabs: "/web/js/comp/Bl_layout",
		Tab: "/web/js/comp/Bl_layout",
		Tabpanels: "/web/js/comp/Bl_layout",
		Tabpanel: "/web/js/comp/Bl_layout",
		//Tree
		Tree: "/web/js/comp/Tree",
		ComboTree: "/web/js/comp/ComboTree",
		//MdLoading
		MdLoading: "/web/js/comp/OtherCom",
		//Notice
		Notice: "/web/js/comp/OtherCom",
		//Accordion
		Accordion: "/web/js/comp/Bl_layout",
		//上传控件
		UploadButton: "/web/js/comp/OtherCom",
		AutoComplete: "/web/js/comp/OtherCom",
		
		InputElement: "/web/js/comp/InputElement",
		TextInput: "/web/js/comp/InputElement",
		Date: "/web/js/comp/InputElement",
		Select: "/web/js/comp/InputElement",
		Checkbox: "/web/js/comp/InputElement",
		Number: "/web/js/comp/InputElement",
		
		//确认框
		ConFirm: "/web/sys/ConFirm",
		SysWin: "/web/sys/SysWin",
		//mainFrame
		MainFrame: "/web/sys/MainFrame"
	}
});
