{Component} = require "Component"

exports.InputElement = new Class(
  Extends: Component
  onDraw: ->
    t = this
    o = t.options
    elt = o.ele
    oldVal = t.getVal()
    t.setOldVal oldVal
    return
  getOldVal: ->
    t = this
    o = t.options
    elt = o.ele
    val = elt.retrieve "oldValue"
    val
  #设置默认值
  setOldVal: (val)->
    t = this
    o = t.options
    elt = o.ele
    val = String val
    elt.eliminate "oldValue"
    elt.store "oldValue",val
    t
  "@getVal":{comment:"""
    获取控件的值,默认跟旧值比较,如果跟旧值oldVal相等,则返回undefined,否则返回当前控件的值
      若opt.cmpOldVal 为 false,则代表不跟oldval比较,直接返回当前控件的值
  """}
  getVal: (opt)->
    t = this
    o = t.options
    elt = o.ele
    val = elt.get "value"
    if !opt or opt.cmpOldVal isnt false
      oldVal = t.getOldVal()
      return if val is oldVal
    val
  setVal: (val)->
    t = this
    o = t.options
    elt = o.ele
    val = elt.set "value",val
    t
)
InputElement = exports.InputElement

exports.TextInput = new Class
  Extends: InputElement
TextInput = exports.TextInput

exports.Date = new Class
  Extends: InputElement
Date = exports.Date

exports.Number = new Class
  Extends: InputElement
Number = exports.Number

exports.Select = new Class
  Extends: InputElement
Select = exports.Select

exports.Checkbox = new Class(
  Extends: InputElement
  getOldVal: ->
    t = this
    o = t.options
    elt = o.ele
    val = elt.retrieve "oldValue"
    val
  #设置默认值
  setOldVal: (val)->
    t = this
    o = t.options
    elt = o.ele
    val = Boolean val
    elt.eliminate "oldValue"
    elt.store "oldValue",val
    t
  getVal: (opt)->
    t = this
    o = t.options
    elt = o.ele
    val = elt.checked
    if !opt or opt.cmpOldVal isnt false
      oldVal = t.getOldVal()
      return if val is oldVal
    val
  setVal: (val)->
    t = this
    o = t.options
    elt = o.ele
    val = Boolean val
    elt.checked = val
    t
)
Checkbox = exports.Checkbox